import requests as rq
from requests_html import HTMLSession
import subprocess

selectors = {
    # First-Level-Page: contain all platform urls
    "platform_selector":{
        "find":"body > div > div > span > a",
        "attr":"href"
    },
    "video_selector_blue":{
        "find":"form > input[type=hidden]:nth-child(2)",
        "attr":"value"
    },
    "video_selector_yellow":{
        "find":".modal.fade > div > div > div.modal-body",
        "attr":""
    },
}

def addRootPath(links):
    return list(map(lambda url: "https://www.258s.net/"+url, links)) # add root-url-path

def removeDefaultLink(video_links):
    return [ link for link in video_links if not "pull.bcqupfu.cn" in link ]

def getLinks(resp, selector, middlewares):
    links = resp.html.find(selector["find"])

    if selector["attr"] != "":
        links = list(map(lambda link:link.attrs[selector["attr"]], links))
    else:
        links = list(map(lambda link:link.text.split("：\n")[1], links))

    # 執行中介的處理函數
    if len(middlewares) !=0 :
        for fn in middlewares:
            links = fn(links)

    return links

def verifyLinks(videoLinks):
    total = len(videoLinks)

    for index, link in enumerate(videoLinks):
        print(f"{index} / {total}")
        timeout = 3

        cmd = [ "ffprobe","-v", "panic","-show_format", link, "-listen_timeout", str(timeout)]

        try:
            check = subprocess.check_output(cmd, timeout=timeout)
        except Exception as e:
            print(f"[remove] {link}")
            del videoLinks[index]

    return videoLinks

def pp(list):
    for item in list:
        print(item)

def main():
    session = HTMLSession()
    resp = session.get("https://www.258s.net/", verify=False)

    # 取得平台網頁的網址
    platform_links = getLinks(resp, selectors["platform_selector"], middlewares=[addRootPath])

    # 取得串流源的網址
    links = []
    for link in platform_links[9:11]:
        resp = session.get(link, verify=False)

        # 針對網頁中的藍色按鈕，for rtmp-request
        video_links = getLinks(resp, selectors["video_selector_blue"], middlewares=[removeDefaultLink])
        links.extend(video_links)

        #針對網頁中的黃色按鈕，for http-request
        video_links = getLinks(resp, selectors["video_selector_yellow"], middlewares=[])
        links.extend(video_links)

    links = set(verifyLinks(links))

    print("======= available links =======\n")
    pp(links)

if __name__ == "__main__":
    main()
