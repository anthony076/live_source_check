
# Function
 - Grab live-video source from webpage
 - Filter invalid URL
 - Verify the video is available via ffprobe library
 - Output result into text file

# Stack
 - request-html
 - subprocess

 # Todos
 - use thread/async to conduct video verify command
 - implement text file output
